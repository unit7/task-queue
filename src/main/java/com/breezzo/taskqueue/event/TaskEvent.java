package com.breezzo.taskqueue.event;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * Событие для обработки. Содержит дату, время, в которое необходимо запустить задачу.
 *
 * Created by breezzo on 21.07.16.
 */
public class TaskEvent {
    private Date startDate;
    private Callable task;

    /**
     * @param startDate дата, время запуска задачи
     * @param task задача
     */
    public static TaskEvent of(Date startDate, Callable task) {
        TaskEvent event = new TaskEvent();
        event.setStartDate(startDate);
        event.setTask(task);
        return event;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Callable getTask() {
        return task;
    }

    public void setTask(Callable task) {
        this.task = task;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TaskEvent{");
        sb.append("startDate=").append(startDate);
        sb.append('}');
        return sb.toString();
    }
}
