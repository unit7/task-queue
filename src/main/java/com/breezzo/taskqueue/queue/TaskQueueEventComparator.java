package com.breezzo.taskqueue.queue;

import com.breezzo.taskqueue.event.TaskEvent;

import java.util.Comparator;

/**
 * Гарантирует выполнение задач в порядке добавления в очередь, если их время запуска одинаковое
 *
 * Created by breezzo on 21.07.16.
 */
public class TaskQueueEventComparator implements Comparator<TaskQueueEvent> {
    @Override
    public int compare(TaskQueueEvent o1, TaskQueueEvent o2) {
        TaskEvent taskEvent1 = o1.getTaskEvent();
        TaskEvent taskEvent2 = o2.getTaskEvent();

        int res = taskEvent1.getStartDate().compareTo(taskEvent2.getStartDate());
        if (res != 0)
            return res;

        return (int) (o1.getOrder() - o2.getOrder());
    }
}
