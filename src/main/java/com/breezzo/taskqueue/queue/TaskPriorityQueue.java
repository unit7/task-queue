package com.breezzo.taskqueue.queue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Реализация очереди на очереди с приоритетами
 *
 * Created by breezzo on 21.07.16.
 */
@ThreadSafe
public class TaskPriorityQueue implements TaskQueue {

    private PriorityBlockingQueue<TaskQueueEvent> eventQueue;

    public TaskPriorityQueue(Comparator<TaskQueueEvent> cmp) {
        eventQueue = new PriorityBlockingQueue<>(11, cmp);
    }

    @Override
    public void add(@Nonnull TaskQueueEvent taskEvent) {
        eventQueue.add(taskEvent);
    }

    @Nullable
    @Override
    public TaskQueueEvent poll(Date onDate) {
        TaskQueueEvent head = eventQueue.poll();
        if (head != null && !head.getTaskEvent().getStartDate().after(onDate)) {
            return head;
        } else if (head != null) {
            eventQueue.add(head);
        }

        return null;
    }
}
