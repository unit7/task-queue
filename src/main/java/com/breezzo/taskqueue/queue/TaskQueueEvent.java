package com.breezzo.taskqueue.queue;

import com.breezzo.taskqueue.event.TaskEvent;

/**
 * Обертка над событием для сортировки в порядке добавления в очередь
 *
 * Created by breezzo on 21.07.16.
 */
public class TaskQueueEvent {
    private TaskEvent taskEvent;
    private long order;

    public static TaskQueueEvent of(TaskEvent event, long order) {
        TaskQueueEvent instance = new TaskQueueEvent();
        instance.setTaskEvent(event);
        instance.setOrder(order);
        return instance;
    }

    public TaskEvent getTaskEvent() {
        return taskEvent;
    }

    public void setTaskEvent(TaskEvent taskEvent) {
        this.taskEvent = taskEvent;
    }

    public long getOrder() {
        return order;
    }

    public void setOrder(long order) {
        this.order = order;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TaskQueueEvent{");
        sb.append("taskEvent=").append(taskEvent);
        sb.append(", order=").append(order);
        sb.append('}');
        return sb.toString();
    }
}
