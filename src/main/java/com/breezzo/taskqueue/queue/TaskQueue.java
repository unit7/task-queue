package com.breezzo.taskqueue.queue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

/**
 * Интерфейс очереди сообщений
 *
 * Created by breezzo on 21.07.16.
 */
public interface TaskQueue {
    /**
     * Добавить событие в очередь
     */
    void add(@Nonnull TaskQueueEvent taskEvent);

    /**
     * Получить сообщение из очереди не позднее указанной даты
     */
    @Nullable
    TaskQueueEvent poll(Date onDate);
}
