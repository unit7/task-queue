package com.breezzo.taskqueue.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Реализация очереди на множестве, основанном на списке с пропусками
 *
 * Created by breezzo on 21.07.16.
 */
@ThreadSafe
public class TaskQueueSkipListSet implements TaskQueue {

    private static final Logger logger = LoggerFactory.getLogger(TaskQueueSkipListSet.class);

    private final ConcurrentSkipListSet<TaskQueueEvent> eventQueue;

    public TaskQueueSkipListSet(Comparator<TaskQueueEvent> cmp) {
        this.eventQueue = new ConcurrentSkipListSet<>(cmp);
    }

    @Override
    public void add(@Nonnull TaskQueueEvent taskEvent) {
        logger.trace("Adding event {}", taskEvent);
        eventQueue.add(taskEvent);
    }

    @Nullable
    @Override
    public TaskQueueEvent poll(Date onDate) {
        TaskQueueEvent earlyEvent = eventQueue.pollFirst();
        if (earlyEvent != null) {
            if (!earlyEvent.getTaskEvent().getStartDate().after(onDate)) {
                logger.trace("Event polled {}", earlyEvent);
                return earlyEvent;
            } else {
                eventQueue.add(earlyEvent);
            }
        }

        return null;
    }
}
