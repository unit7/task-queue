package com.breezzo.taskqueue.executor;

import com.breezzo.taskqueue.event.TaskEvent;

import javax.annotation.Nonnull;

/**
 * Интерфейс, через который происходит добавление событий в очередь для последующей обработки.
 *
 * Created by breezzo on 21.07.16.
 */
public interface TaskQueueExecutor {

    /**
     * Добавить событие в очередь
     * @param taskEvent событие
     */
    void addTask(@Nonnull TaskEvent taskEvent);
}
