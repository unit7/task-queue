package com.breezzo.taskqueue.executor;

import com.breezzo.taskqueue.event.TaskEvent;
import com.breezzo.taskqueue.mapper.ExceptionMapper;
import com.breezzo.taskqueue.queue.TaskQueue;
import com.breezzo.taskqueue.queue.TaskQueueEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Класс выполняет асинхронную обработку входящих событий:
 *  добавляет входящее событие в очередь
 *  периодически просматривает очередь на предмет появления задачи, которую необходимо выполнить в текущий момент.
 *
 * Created by breezzo on 21.07.16.
 */
@ThreadSafe
public class AsyncTaskExecutor implements TaskQueueExecutor {
    private static final Logger logger = LoggerFactory.getLogger(AsyncTaskExecutor.class);

    public static final long DEFAULT_SLEEP_TIME_MS = 100;

    private ExecutorService executorService;

    private final TaskQueue taskQueue;
    private final ExceptionMapper exceptionMapper;

    private final long delay;
    private final TimeUnit delayTimeUnit;

    /**
     * Для количества событий на одну единицу времени инта должно хватить :)
     */
    private final AtomicInteger order = new AtomicInteger(0);
    private AtomicBoolean running = new AtomicBoolean(false);

    /**
     * @param taskQueue реализация очереди, в которую будут поступать события
     * @param exceptionMapper реализация обработчика ошибок, которые могут возникнуть при выполнении задачи
     */
    public AsyncTaskExecutor(@Nonnull TaskQueue taskQueue, @Nonnull ExceptionMapper exceptionMapper) {
        this(taskQueue, exceptionMapper, DEFAULT_SLEEP_TIME_MS, TimeUnit.MILLISECONDS, null);
    }

    /**
     *
     * @param taskQueue реализация очереди, в которую будут поступать события
     * @param exceptionMapper реализация обработчика ошибок, которые могут возникнуть при выполнении задачи
     * @param delay период просмотра очереди событий, по умолчанию {@link AsyncTaskExecutor#DEFAULT_SLEEP_TIME_MS}
     * @param delayTimeUnit единица измерения времени для периода просмотра очереди
     * @param executorService опциональный пул, на котором можно запустить обработку очереди. По умолчанию поток создается на общем пуле.
     */
    public AsyncTaskExecutor(@Nonnull TaskQueue taskQueue,
                             @Nonnull ExceptionMapper exceptionMapper,
                             long delay,
                             @Nonnull TimeUnit delayTimeUnit,
                             @Nullable ExecutorService executorService) {

        Objects.requireNonNull(taskQueue);
        Objects.requireNonNull(exceptionMapper);
        Objects.requireNonNull(delayTimeUnit);

        this.taskQueue = taskQueue;
        this.exceptionMapper = exceptionMapper;
        this.executorService = executorService;
        this.delay = delay;
        this.delayTimeUnit = delayTimeUnit;
    }

    @Override
    public void addTask(@Nonnull TaskEvent taskEvent) {
        Objects.requireNonNull(taskEvent);
        this.taskQueue.add(TaskQueueEvent.of(taskEvent, order.incrementAndGet()));
    }

    public void runAsync() {
        if (!running.compareAndSet(false, true)) {
            return;
        }

        if (executorService == null) {
            new Thread(new QueueWatcher()).start();
        } else {
            executorService.submit(new QueueWatcher());
        }
    }

    /**
     * Устанавливает флаг для остановки обработки событий
     */
    public void setStopped() {
        running.set(false);
    }

    /**
     * Инкапсулирует логику обработки очереди
     */
    private final class QueueWatcher implements Runnable {

        @Override
        public void run() {
            logger.info("Start queue watching for events...");

            while (watchQueue()) {
                try {
                    delayTimeUnit.sleep(delay);
                } catch (InterruptedException e) {
                    break;
                }

                if (wasStopped()) {
                    break;
                }
            }
        }

        private boolean watchQueue() {
            TaskQueueEvent event;
            while ((event = taskQueue.poll(new Date())) != null) {
                TaskEvent taskEvent = event.getTaskEvent();

                logger.trace("Start executing task on date {} with order {}", taskEvent.getStartDate(), event.getOrder());

                Object result = executeTask(taskEvent);

                logger.trace("Task on date {} with order {} executed. result: {}",
                        taskEvent.getStartDate(),
                        event.getOrder(),
                        result);

                if (wasStopped()) {
                    return false;
                }
            }

            return true;
        }

        private Object executeTask(TaskEvent event) {
            try {
                return event.getTask().call();
            } catch (Exception e) {
                exceptionMapper.map(event, e);
                return null;
            }
        }

        private boolean wasStopped() {
            if (!running.get() || Thread.currentThread().isInterrupted()) {
                logger.info("Queue watching was stopped. Exiting...");
                return true;
            }

            return false;
        }
    }
}
