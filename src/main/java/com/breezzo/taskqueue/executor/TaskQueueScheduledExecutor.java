package com.breezzo.taskqueue.executor;

import com.breezzo.taskqueue.event.TaskEvent;
import com.breezzo.taskqueue.mapper.ExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Данная реализация основана на {@link ScheduledExecutorService}.
 * Все добавляемые события буду отправлены в этот пул на выполнение в указанное время.
 *
 * Created by breezzo on 23.07.16.
 */
public class TaskQueueScheduledExecutor implements TaskQueueExecutor {

    private static final Logger logger = LoggerFactory.getLogger(TaskQueueScheduledExecutor.class);

    private final ExceptionMapper exceptionMapper;
    private final ScheduledExecutorService executor;

    /**
     * @param exceptionMapper перехватывает ошибки, происходящие при выполнении задачи
     */
    public TaskQueueScheduledExecutor(ExceptionMapper exceptionMapper) {
        this.exceptionMapper = exceptionMapper;
        this.executor = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void addTask(@Nonnull TaskEvent taskEvent) {
        Objects.requireNonNull(taskEvent);
        logger.trace("add task {}", taskEvent);

        Date startDate = taskEvent.getStartDate();

        long delay = Math.max(0, startDate.getTime() - System.currentTimeMillis());
        executor.schedule(new SafeTaskWrapper(exceptionMapper, taskEvent), delay, TimeUnit.MILLISECONDS);
    }

    public void initializeStop() {
        executor.shutdown();
    }

    private static final class SafeTaskWrapper implements Runnable {

        private ExceptionMapper exceptionMapper;
        private TaskEvent taskEvent;

        private SafeTaskWrapper(ExceptionMapper exceptionMapper, TaskEvent taskEvent) {
            this.exceptionMapper = exceptionMapper;
            this.taskEvent = taskEvent;
        }

        @Override
        public void run() {
            try {
                taskEvent.getTask().call();
            } catch (Exception e) {
                exceptionMapper.map(taskEvent, e);
            }
        }
    }
}
