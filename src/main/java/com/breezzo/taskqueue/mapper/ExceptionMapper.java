package com.breezzo.taskqueue.mapper;

import com.breezzo.taskqueue.event.TaskEvent;

/**
 * Обрабатывает ошибки, происходящие при выполнении задачи
 *
 * Created by breezzo on 22.07.16.
 */
public interface ExceptionMapper {
    void map(TaskEvent event, Exception e);
}
