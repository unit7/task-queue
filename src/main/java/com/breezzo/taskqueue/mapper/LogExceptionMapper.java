package com.breezzo.taskqueue.mapper;

import com.breezzo.taskqueue.event.TaskEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by breezzo on 22.07.16.
 */
public class LogExceptionMapper implements ExceptionMapper {
    private static final Logger logger = LoggerFactory.getLogger(LogExceptionMapper.class);

    @Override
    public void map(TaskEvent event, Exception e) {
        logger.error("Error occurred for event " + event, e);
    }
}
