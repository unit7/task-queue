package com.breezzo.taskqueue.jmh;

import com.breezzo.taskqueue.CallableStub;
import com.breezzo.taskqueue.event.TaskEvent;
import com.breezzo.taskqueue.executor.TaskQueueScheduledExecutor;
import com.breezzo.taskqueue.mapper.LogExceptionMapper;
import org.openjdk.jmh.annotations.*;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by breezzo on 21.07.16.
 */
@State(Scope.Benchmark)
@Warmup(iterations = 3)
@Fork(value = 1)
@Measurement(iterations = 10, time = 500, timeUnit = TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.AverageTime)
@Threads(4)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class ScheduledExecutorJmhTest {

    private TaskQueueScheduledExecutor taskQueueExecutor;

    @Setup
    public void setup() {
        taskQueueExecutor = new TaskQueueScheduledExecutor(new LogExceptionMapper());
    }

    @TearDown
    public void tearDown() {
        taskQueueExecutor.initializeStop();
    }

    @Benchmark
    public void scheduledExecutor() throws InterruptedException {
        Date date = new Date();
        for (int i = 0; i < 100000; ++i) {
            taskQueueExecutor.addTask(TaskEvent.of(date, new CallableStub()));
        }

        CallableStub task = new CallableStub();
        taskQueueExecutor.addTask(TaskEvent.of(date, task));

        while (!task.isWasCalled()) {
            TimeUnit.MILLISECONDS.sleep(10);
        }
    }
}
