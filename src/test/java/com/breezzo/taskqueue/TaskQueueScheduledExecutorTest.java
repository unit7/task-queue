package com.breezzo.taskqueue;

import com.breezzo.taskqueue.event.TaskEvent;
import com.breezzo.taskqueue.executor.TaskQueueScheduledExecutor;
import com.breezzo.taskqueue.mapper.ExceptionMapper;
import com.breezzo.taskqueue.mapper.LogExceptionMapper;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by breezzo on 23.07.16.
 */
public class TaskQueueScheduledExecutorTest {

    @Test
    public void exceptionTest() throws InterruptedException {

        final String exceptionMsg = "call_exception";
        String[] resultExceptionMsg = new String[] { "" };

        TaskQueueScheduledExecutor executor = new TaskQueueScheduledExecutor(new ExceptionMapper() {
            @Override
            public void map(TaskEvent event, Exception e) {
                resultExceptionMsg[0] = e.getMessage();
            }
        });

        executor.addTask(TaskEvent.of(new Date(), new Callable() {
            @Override
            public Object call() throws Exception {
                throw new Exception(exceptionMsg);
            }
        }));

        TimeUnit.MILLISECONDS.sleep(300);

        Assert.assertEquals(exceptionMsg, resultExceptionMsg[0]);
    }

    @Test
    public void orderTest() throws InterruptedException {

        TaskQueueScheduledExecutor executor = new TaskQueueScheduledExecutor(new LogExceptionMapper());

        List<Integer> resultOrder = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        calendar.add(Calendar.SECOND, 1);

        // third
        TaskEvent event = TaskEvent.of(calendar.getTime(), () -> {
            resultOrder.add(0);
            return 0;
        });

        executor.addTask(event);

        calendar.add(Calendar.SECOND, -1);

        // first
        event = TaskEvent.of(calendar.getTime(), () -> {
            resultOrder.add(1);
            return 0;
        });

        executor.addTask(event);

        // second
        event = TaskEvent.of(calendar.getTime(), () -> {
            resultOrder.add(2);
            return 0;
        });

        executor.addTask(event);

        calendar.add(Calendar.SECOND, 2);

        // fourth
        event = TaskEvent.of(calendar.getTime(), () -> {
            resultOrder.add(3);
            return 0;
        });

        executor.addTask(event);

        TimeUnit.SECONDS.sleep(3);

        Assert.assertEquals(Arrays.asList(1, 2, 0, 3), resultOrder);
    }

    @Test(expected = RejectedExecutionException.class)
    public void stopTest() throws InterruptedException {
        TaskQueueScheduledExecutor executor = new TaskQueueScheduledExecutor(new LogExceptionMapper());

        CallableStub task = new CallableStub();
        executor.addTask(TaskEvent.of(new Date(), task));

        task = new CallableStub();
        executor.addTask(TaskEvent.of(new Date(), task));

        executor.initializeStop();
        TimeUnit.MILLISECONDS.sleep(300);

        Assert.assertTrue(task.isWasCalled());

        task = new CallableStub();
        executor.addTask(TaskEvent.of(new Date(), task));
    }
}
