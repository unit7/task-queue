package com.breezzo.taskqueue;

import com.breezzo.taskqueue.event.TaskEvent;
import com.breezzo.taskqueue.executor.AsyncTaskExecutor;
import com.breezzo.taskqueue.executor.TaskQueueExecutor;
import com.breezzo.taskqueue.mapper.LogExceptionMapper;
import com.breezzo.taskqueue.queue.TaskQueueEventComparator;
import com.breezzo.taskqueue.queue.TaskQueueSkipListSet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Created by breezzo on 21.07.16.
 */
public class TaskQueueExecutorTest {

    private TaskQueueExecutor taskQueueExecutor;

    @Before
    public void setup() {
        AsyncTaskExecutor asyncTaskExecutor = new AsyncTaskExecutor(
                new TaskQueueSkipListSet(new TaskQueueEventComparator()), new LogExceptionMapper()
        );
        asyncTaskExecutor.runAsync();
        taskQueueExecutor = asyncTaskExecutor;
    }

    @Test
    public void callableExecutes() throws InterruptedException {
        CallableStub task = new CallableStub();
        TaskEvent event = TaskEvent.of(new Date(), task);

        taskQueueExecutor.addTask(event);

        TimeUnit.MILLISECONDS.sleep(1000);

        Assert.assertTrue(task.isWasCalled());
    }

    @Test
    public void executesSequence() throws InterruptedException {
        final int intervalMs = 10;
        final int sequenceLength = 100;

        int[] resultArray = new int[sequenceLength];
        Arrays.fill(resultArray, -1);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        for (int i = 0; i < sequenceLength; ++i) {
            final int index = i;
            Callable<Integer> callable = () -> {
                resultArray[index] = index;
                return 0;
            };
            Date eventStartTime = calendar.getTime();
            TaskEvent event = TaskEvent.of(eventStartTime, callable);
            taskQueueExecutor.addTask(event);

            calendar.add(Calendar.MILLISECOND, intervalMs);
        }

        TimeUnit.MILLISECONDS.sleep(sequenceLength * intervalMs + 1000);

        for (int i = 0; i < resultArray.length; i++) {
            Assert.assertEquals(i, resultArray[i]);
        }
    }

    @Test
    public void executesOrdered() throws InterruptedException {
        List<Integer> resultOrder = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        calendar.add(Calendar.SECOND, 2);

        // third
        TaskEvent event = TaskEvent.of(calendar.getTime(), () -> {
            resultOrder.add(0);
            return 0;
        });

        taskQueueExecutor.addTask(event);

        calendar.add(Calendar.SECOND, -2);

        // first
        event = TaskEvent.of(calendar.getTime(), () -> {
            resultOrder.add(1);
            return 0;
        });

        taskQueueExecutor.addTask(event);

        // second
        event = TaskEvent.of(calendar.getTime(), () -> {
            resultOrder.add(2);
            return 0;
        });

        taskQueueExecutor.addTask(event);

        calendar.add(Calendar.SECOND, 3);

        // fourth
        event = TaskEvent.of(calendar.getTime(), () -> {
            resultOrder.add(3);
            return 0;
        });

        taskQueueExecutor.addTask(event);

        TimeUnit.SECONDS.sleep(4);

        Assert.assertEquals(Arrays.asList(1, 2, 0, 3), resultOrder);
    }
}
