package com.breezzo.taskqueue;

import java.util.concurrent.Callable;

/**
 * Created by breezzo on 22.07.16.
 */
public class CallableStub implements Callable<Integer> {
    private boolean wasCalled;

    @Override
    public Integer call() throws Exception {
        wasCalled = true;
        return 0;
    }

    public boolean isWasCalled() {
        return wasCalled;
    }
}
