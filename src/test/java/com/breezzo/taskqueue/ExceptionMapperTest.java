package com.breezzo.taskqueue;

import com.breezzo.taskqueue.event.TaskEvent;
import com.breezzo.taskqueue.executor.AsyncTaskExecutor;
import com.breezzo.taskqueue.queue.TaskQueueEventComparator;
import com.breezzo.taskqueue.queue.TaskQueueSkipListSet;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by breezzo on 22.07.16.
 */
public class ExceptionMapperTest {
    private static final String EXCEPTION_MSG = "call_exception";

    @Test
    public void exceptionMapped() throws InterruptedException {
        final String[] result = new String[] { "" };

        AsyncTaskExecutor asyncTaskExecutor = new AsyncTaskExecutor(
                new TaskQueueSkipListSet(new TaskQueueEventComparator()),
                (event, e) -> {
                    result[0] = e.getMessage();
                }
        );
        asyncTaskExecutor.runAsync();

        asyncTaskExecutor.addTask(TaskEvent.of(new Date(), () -> {
            throw new Exception(EXCEPTION_MSG);
        }));

        TimeUnit.MILLISECONDS.sleep(200);

        Assert.assertEquals(EXCEPTION_MSG, result[0]);
    }
}
