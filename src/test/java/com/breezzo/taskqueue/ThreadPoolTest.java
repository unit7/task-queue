package com.breezzo.taskqueue;

import com.breezzo.taskqueue.event.TaskEvent;
import com.breezzo.taskqueue.executor.AsyncTaskExecutor;
import com.breezzo.taskqueue.mapper.LogExceptionMapper;
import com.breezzo.taskqueue.queue.TaskQueueEventComparator;
import com.breezzo.taskqueue.queue.TaskQueueSkipListSet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by breezzo on 22.07.16.
 */
public class ThreadPoolTest {

    private ExecutorService executorService;
    private AsyncTaskExecutor taskQueueExecutor;

    @Before
    public void setup() {
        executorService = Executors.newFixedThreadPool(1);
        taskQueueExecutor = new AsyncTaskExecutor(
                new TaskQueueSkipListSet(new TaskQueueEventComparator()),
                new LogExceptionMapper(),
                AsyncTaskExecutor.DEFAULT_SLEEP_TIME_MS, TimeUnit.MILLISECONDS,
                executorService
        );
        taskQueueExecutor.runAsync();
    }

    @Test
    public void testApiStop() throws InterruptedException {
        taskQueueExecutor.addTask(TaskEvent.of(new Date(), new CallableStub()));
        taskQueueExecutor.setStopped();

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.SECONDS);

        CallableStub task = new CallableStub();
        taskQueueExecutor.addTask(TaskEvent.of(new Date(), task));

        TimeUnit.MILLISECONDS.sleep(300);

        Assert.assertFalse(task.isWasCalled());
    }

    @Test
    public void testInterrupted() throws InterruptedException {
        taskQueueExecutor.addTask(TaskEvent.of(new Date(), new CallableStub()));

        executorService.shutdownNow();

        CallableStub task = new CallableStub();
        taskQueueExecutor.addTask(TaskEvent.of(new Date(), task));

        TimeUnit.MILLISECONDS.sleep(300);

        Assert.assertFalse(task.isWasCalled());
    }
}
